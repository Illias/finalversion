﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialNetwork.Models;

namespace SocialNetwork.Controllers
{
    public class ConvUsersController : Controller
    {
        private Database1Entities1 db = new Database1Entities1();

        // GET: ConvUsers
        public ActionResult Index()
        {
            var convUsers = db.ConvUsers.Include(c => c.Conversation).Include(c => c.User);
            return View(convUsers.ToList());
        }

        // GET: ConvUsers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConvUser convUser = db.ConvUsers.Find(id);
            if (convUser == null)
            {
                return HttpNotFound();
            }
            return View(convUser);
        }

        // GET: ConvUsers/Create
        public ActionResult Create()
        {
            ViewBag.ConvId = new SelectList(db.Conversations, "Id", "Name");
            ViewBag.UserId = new SelectList(db.Users, "Id", "login");
            return View();
        }

        // POST: ConvUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ConvId,UserId")] ConvUser convUser)
        {
            if (ModelState.IsValid)
            {
                db.ConvUsers.Add(convUser);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ConvId = new SelectList(db.Conversations, "Id", "Name", convUser.ConvId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "login", convUser.UserId);
            return View(convUser);
        }

        // GET: ConvUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConvUser convUser = db.ConvUsers.Find(id);
            if (convUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.ConvId = new SelectList(db.Conversations, "Id", "Name", convUser.ConvId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "login", convUser.UserId);
            return View(convUser);
        }

        // POST: ConvUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ConvId,UserId")] ConvUser convUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(convUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ConvId = new SelectList(db.Conversations, "Id", "Name", convUser.ConvId);
            ViewBag.UserId = new SelectList(db.Users, "Id", "login", convUser.UserId);
            return View(convUser);
        }

        // GET: ConvUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ConvUser convUser = db.ConvUsers.Find(id);
            if (convUser == null)
            {
                return HttpNotFound();
            }
            return View(convUser);
        }

        // POST: ConvUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ConvUser convUser = db.ConvUsers.Find(id);
            db.ConvUsers.Remove(convUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
