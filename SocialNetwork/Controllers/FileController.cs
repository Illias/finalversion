﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialNetwork.Models;
using System.Data.Entity;
using System.Data;

namespace SocialNetwork.Controllers
{
    public class FileController : Controller
    {
        private Database1Entities1 db = new Database1Entities1();
        // GET: File
        public int Create(string url)
        {
            File file = new File();
            file.Url = url;
            db.Files.Add(file);
            db.SaveChanges();
            return file.Id;
        }
        

    }
}