﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialNetwork.Models;

namespace SocialNetwork.Controllers
{
    public class FriendRequestsController : Controller
    {
        private Database1Entities1 db = new Database1Entities1();

        // GET: FriendRequests
        public ActionResult Index()
        {
            return View(db.FriendRequests.ToList());
        }

        // GET: FriendRequests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FriendRequest friendRequest = db.FriendRequests.Find(id);
            if (friendRequest == null)
            {
                return HttpNotFound();
            }
            return View(friendRequest);
        }

        // GET: FriendRequests/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FriendRequests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,user1,user2")] FriendRequest friendRequest)
        {
            if (ModelState.IsValid)
            {
                db.FriendRequests.Add(friendRequest);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(friendRequest);
        }

        // GET: FriendRequests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FriendRequest friendRequest = db.FriendRequests.Find(id);
            if (friendRequest == null)
            {
                return HttpNotFound();
            }
            return View(friendRequest);
        }

        // POST: FriendRequests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,user1,user2")] FriendRequest friendRequest)
        {
            if (ModelState.IsValid)
            {
                db.Entry(friendRequest).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(friendRequest);
        }

        // GET: FriendRequests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FriendRequest friendRequest = db.FriendRequests.Find(id);
            if (friendRequest == null)
            {
                return HttpNotFound();
            }
            return View(friendRequest);
        }

        // POST: FriendRequests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FriendRequest friendRequest = db.FriendRequests.Find(id);
            db.FriendRequests.Remove(friendRequest);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
