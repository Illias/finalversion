﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialNetwork.Models;
using System.Data;
using System.Data.Entity;

namespace SocialNetwork.Controllers
{
    public class PostsController : Controller
    {
        private Database1Entities1 db = new Database1Entities1();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post)
        {
            if (ModelState.IsValid)
            {
                post.UserId = int.Parse(Session["UserID"].ToString());
                db.Posts.Add(post);
                db.SaveChanges();
            }
            return RedirectToAction("Index","Home");
        }

    }
}