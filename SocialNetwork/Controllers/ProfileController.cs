﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialNetwork.Models;
using System.Data;
using System.Data.Entity;
using System.Net;
using SocialNetwork.Controllers;

namespace SocialNetwork.Controllers
{
    public class ProfileController : Controller
    {
        // GET: Profile
        private Database1Entities1 db = new Database1Entities1();
        public ActionResult Create()
        {
            if (ModelState.IsValid)
            {
               
                Profile profile = new Models.Profile();
                profile.UserId = Int32.Parse(Session["UserId"].ToString());
                profile.FirstName = "Your Name";
                profile.LastName = "Your last name";
                profile.gender = "male";
                profile.BirthDate = new DateTime().Date;
                db.Profiles.Add(profile);
                db.SaveChanges();
                return RedirectToAction("index", "Home");
            }

            return View();
        }
       

        public ActionResult Edit(int? uId)
        {
            if (Session["UserID"] != null)
            {
                if (uId == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                if (int.Parse(Session["UserID"].ToString()) != uId)
                {
                    return HttpNotFound();
                }
                Profile profile = db.Profiles.Where(x => x.UserId == uId).FirstOrDefault();
                if (profile == null)
                {
                    return HttpNotFound();
                }
                return View(profile);
            }
            return HttpNotFound();

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Profile profile)
        {
            if (ModelState.IsValid)
            {
                profile.UserId = Int32.Parse(Session["UserId"].ToString());
                db.Entry(profile).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(profile);
        }
    }
}