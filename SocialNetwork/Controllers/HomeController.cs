﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using SocialNetwork.Models;

namespace SocialNetwork.Controllers
{
    public class HomeController : Controller
    {
        private Database1Entities1 db = new Database1Entities1();
        // GET: Home
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            User user = db.Users.Find(Int32.Parse(Session["UserID"].ToString()));
            Profile profile = db.Profiles.Where(x => x.UserId == user.Id).FirstOrDefault();

            ViewBag.my = true;
            ViewBag.UserId = user.Id;
            ViewBag.profileid = profile.Id;
            ViewBag.ProfileName = profile.FirstName;
            ViewBag.ProfileSurName = profile.LastName;
            ViewBag.image = db.Files.Find(user.AvatarID).Url;
            return View(user.Posts.Reverse());
        }

        [HttpPost]
        public ActionResult Index(int id)
        {
            if (Session["UserID"] == null)
            {
                return RedirectToAction("Login", "Users");
            }
            User user = db.Users.Find(id);
            Profile profile = db.Profiles.Where(x => x.UserId == user.Id).FirstOrDefault();

            ViewBag.my = false;
            ViewBag.UserId = user.Id;
            ViewBag.profileid = profile.Id;
            ViewBag.ProfileName = profile.FirstName;
            ViewBag.ProfileSurName = profile.LastName;
            ViewBag.image = db.Files.Find(user.AvatarID).Url;
            return View(user.Posts.Reverse());
        }


        public ActionResult search(string name)
        {
            if (Session["UserID"] == null)
                return HttpNotFound();

            int id = int.Parse(Session["UserID"].ToString());
            ViewBag.userID = id; 
            List<Profile> pfs = db.Profiles.Where(i => i.FirstName.Contains(name) || i.LastName.Contains(name)).ToList();
            Profile me = pfs.Where(i => i.UserId == id).FirstOrDefault();
            if (me != null)
                pfs.Remove(me);

            List<User> users = new List<Models.User>();            

            foreach (var item in pfs)
            {
                if(db.FriendRelations.Where(i => (i.UserId1 == id && i.UserId2 == item.User.Id) 
                        || (i.UserId1 == item.User.Id && i.UserId2 == id) ).Count() == 0 )
                {

                        users.Add(item.User);   
                }
                    
            }

            return View(users);
        }

        public ActionResult friends()
        {

            if (Session["UserID"] == null)
                return RedirectToAction("Index", "Home");

            int id = int.Parse(Session["UserID"].ToString());
            ViewBag.UserId = id;

            var arr = db.FriendRelations.Where(k => k.UserId1 == id || k.UserId2 == id);

            List<User> friendlist = new List<Models.User>();
            foreach (var item in arr)
            {
                if(item.UserId1 != id)
                    friendlist.Add(db.Users.Find(item.UserId1));
                if (item.UserId2 != id)
                    friendlist.Add(db.Users.Find(item.UserId2));
            }
            User me = friendlist.Where(i => i.Id == id).FirstOrDefault(); ;
            if (me != null)
                friendlist.Remove(me);


            List<User> rlist = new List<Models.User>();// new List<FriendRequest>(); 
            var arr2 = db.FriendRequests.Where(i => i.user2 == id).ToList();

            foreach (var item in arr2)
            {
                rlist.Add(db.Users.Find(item.user1));
            }

            return View(new Tuple<List<User>, List<User> >(friendlist, rlist));
        }

        public ActionResult addToFriend(int id)
        {
            int myid = int.Parse(Session["UserID"].ToString());

            FriendRequest check = db.FriendRequests.Where(i => i.user1 == id).FirstOrDefault(); ;

            if(check != null)
            {
                db.FriendRequests.Remove(check);
                db.SaveChanges();

                FriendRelation fr = new FriendRelation();
                fr.UserId1 = myid;
                fr.UserId2 = id;
                db.FriendRelations.Add(fr);
                db.SaveChanges();
                return RedirectToAction("friends", "Home");
            }

            FriendRequest r = new FriendRequest();
            r.user1 = myid;
            r.user2 = id;

            db.FriendRequests.Add(r);
            db.SaveChanges();

            return RedirectToAction("friends", "Home");
        }


        public ActionResult sendMessage(int id, string name)
        {
            int userid = int.Parse(Session["UserID"].ToString()); 
            
            var tmp = db.ConvUsers.Where(i => i.UserId == userid);
            var tmp2 = tmp.Where(i => i.UserId == id);
            foreach (var item in tmp)
            {
                foreach (var item2 in tmp2)
                {
                    if (item.Id == item2.Id)
                        return RedirectToAction("index", "Conversations");
                }
            }

            Profile me = db.Profiles.Where(i => i.UserId == userid).FirstOrDefault();

            Conversation newc = new Conversation();
            newc.Name = name + "--" +me.FirstName;
            db.Conversations.Add(newc);
            db.SaveChanges();
            ConvUser newcu = new ConvUser();
            newcu.ConvId = newc.Id;
            newcu.UserId = id;
            ConvUser newcu2 = new ConvUser();
            newcu2.ConvId = newc.Id;
            newcu2.UserId = userid;
            db.ConvUsers.Add(newcu);
            db.ConvUsers.Add(newcu2);
            db.SaveChanges();

            return RedirectToAction("index", "Conversations");
        }


    }
}