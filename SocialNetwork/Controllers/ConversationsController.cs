﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SocialNetwork.Models;

namespace SocialNetwork.Controllers
{
    public class ConversationsController : Controller
    {
        private Database1Entities1 db = new Database1Entities1();


        public ActionResult GetConversation(int convId)
        {
            var message = db.Messages.Where(i => i.ConvId == convId);
            
            var ConvUsers = db.ConvUsers.Where(i => i.ConvId == convId);
            ViewBag.users = ConvUsers.ToList();
            ViewBag.convId = convId;

            if (message == null)
                return RedirectToAction("index", "Conversations");
            return View(message.ToList());
        }


        // GET: Conversations
        public ActionResult Index()
        {
            if (Session["UserID"] == null)
                return RedirectToAction("Index","Home");

            int userId = int.Parse(Session["UserID"].ToString());

            var arr = db.ConvUsers.Where(i => i.UserId == userId);
            if(arr == null)
            {
                return HttpNotFound();
            }

            List<Conversation> convArr = new List<Conversation>();

            foreach (var item in arr)
            {
                convArr.Add(item.Conversation);//db.Conversations.Find(item,.))
            }


            return View(convArr);

            //return View(db.Conversations.Where(i => i = i.ConvUsers.Where(k => k.UserId == id).ToList()).ToList() );
        }

        // GET: Conversations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Conversation conversation = db.Conversations.Find(id);
            if (conversation == null)
            {
                return HttpNotFound();
            }
            return View(conversation);
        }

        // GET: Conversations/Create
        public ActionResult Create()
        {

            return View();
        }


        [HttpPost]
        public ActionResult saveMessage(string text,int convid)
        {
            Message m = new Message();
            m.Text = text;
            m.ConvId = convid;
            m.Date = DateTime.Now;
            db.Messages.Add(m);
            
            db.SaveChanges();

            return RedirectToAction("index");
        }

        [HttpPost]
        public ActionResult Create2(string name)
        {
            return RedirectToAction("index");
        }

        // POST: Conversations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Conversation conversation)
        {
            if (ModelState.IsValid && conversation.Name != null)
            {                
                db.Conversations.Add(conversation);
                db.SaveChanges();

                ConvUser cu = new ConvUser();
                cu.ConvId = conversation.Id;
                cu.UserId = int.Parse(Session["UserID"].ToString());
                db.ConvUsers.Add(cu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index");
        }

        // GET: Conversations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Conversation conversation = db.Conversations.Find(id);
            if (conversation == null)
            {
                return HttpNotFound();
            }
            return View(conversation);
        }

        // POST: Conversations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Conversation conversation)
        {
            if (ModelState.IsValid)
            {
                db.Entry(conversation).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(conversation);
        }

        // GET: Conversations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Conversation conversation = db.Conversations.Find(id);
            if (conversation == null)
            {
                return HttpNotFound();
            }
            return View(conversation);
        }

        // POST: Conversations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Conversation conversation = db.Conversations.Find(id);
            db.Conversations.Remove(conversation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
