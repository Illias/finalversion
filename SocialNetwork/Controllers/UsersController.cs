﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SocialNetwork.Models;
using System.Data;
using System.Data.Entity;
using System.Net;
using SocialNetwork.Controllers;
using System.Web.SessionState;

namespace SocialNetwork.Controllers
{

    public class UsersController : Controller
    {
        private Database1Entities1 db = new Database1Entities1();
        // GET: Users
        public ActionResult Index()
        {

            if (Session["UserID"] != null)
            {
                var ses = Session["UserID"].ToString();
                ViewBag.UId = ses;
            }
            return View(db.Users.ToList());
        }
        //Autorization
        public ActionResult Login()
        {
            return View();
        }
        //Trying to post
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(User user)
        {
            if (ModelState.IsValid)
            {
                var res = db.Users.Where(x => x.login.Equals(user.login) && x.pass.Equals(user.pass)).FirstOrDefault();
                if (res != null)
                {
                    Session["UserID"] = res.Id.ToString();
                    
                    return RedirectToAction("Index","Home");
                }
            }

            return View();
        }
        public ActionResult LogOut()
        {
            Session.Remove("UserID");
            return RedirectToAction("Index", "Home");
                   
        }


        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(User user)
        {
            if (ModelState.IsValid)
            {
                if (Session["lastFileId"] != null)
                {
                    user.AvatarID = int.Parse(Session["lastFileId"].ToString());
                   
                    Session.Remove("lastFileId");
                }

                db.Users.Add(user);
                db.SaveChanges();
                var res = db.Users.Where(x => x.login.Equals(user.login) && x.pass.Equals(user.pass)).FirstOrDefault();
                if (res != null)
                {
                    Session["UserID"] = res.Id.ToString();
                    
                }
                return RedirectToAction("Create", "Profile");
            }

            return View(user);
        }

        public ActionResult Edit(int? id)
        {
            if (Session["UserID"] != null)
            {
                
                var ses = Session["UserID"].ToString();
                ViewBag.UId = ses;
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                if (int.Parse(ses) == id)
                {
                    User user = db.Users.Find(id);
                    if (user == null)
                    {
                        return HttpNotFound();
                    }
                    return View(user);
                }

            }

            return HttpNotFound();

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserId,login,pass")] User user)
        {
            if (ModelState.IsValid)
            {
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(user);
        }
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return RedirectToAction("Details" + "/" + user.Id, "Profile");
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            User user = db.Users.Find(id);
            Profile profile = db.Profiles.Where(x => x.UserId == user.Id).FirstOrDefault();
            ViewBag.UserID = user.Id;
            if (profile == null)
            {
                return HttpNotFound();
            }
            return View(profile);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            User user = db.Users.Find(id);           
        //  new ProfileController().Delete(user.Id);            
            db.Users.Remove(user);
            db.SaveChanges();
            Session["UserID"] = null;
            return RedirectToAction("Index", "Home");
        }
    }
}